package com.mfsandwich.mfsandwich;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ComponentController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/greeting")
    public String getGreeting() {
        return "Tere!?!";
    }

    @GetMapping("/components")
    public List<Component> getComponents() {
        List<Component> components = jdbcTemplate.query("select * from component", (row, count) -> {
            int componentId = row.getInt("id");
            String componentName = row.getString("name");
            String componentArt = row.getString("component_art");
            String componentPicture = row.getString("picture");
            double componentPrice = row.getDouble("price");
            double componentUnitPrice = row.getDouble("unitPrice");
            double componentKcal = row.getDouble("kCal");
            double componentUnitKcal = row.getDouble("unitkCal");

            Component component = new Component();
            component.setId(componentId);
            component.setName(componentName);
            component.setComponent_art(componentArt);
            component.setPicture(componentPicture);
            component.setPrice(componentPrice);
            component.setUnitPrice(componentUnitPrice);
            component.setKcal(componentKcal);
            component.setUnitKcal(componentUnitKcal);

            return component;
        });
        return components;
    }


}
