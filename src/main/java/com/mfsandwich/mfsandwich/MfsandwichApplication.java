package com.mfsandwich.mfsandwich;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MfsandwichApplication {

	public static void main(String[] args) {
		SpringApplication.run(MfsandwichApplication.class, args);
	}

}

