package com.mfsandwich.mfsandwich;

public class Component {

    private int id;
    private String name;
    private String component_art;
    private String picture;
    private double price;
    private double unitPrice;
    private double Kcal;
    private double unitKcal;

    public String getComponent_art() {
        return component_art;
    }

    public void setComponent_art(String component_art) {
        this.component_art = component_art;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getKcal() {
        return Kcal;
    }

    public void setKcal(double Kcal) {
        this.Kcal = Kcal;
    }

    public double getUnitKcal() {
        return unitKcal;
    }

    public void setUnitKcal(double unitKcal) {
        this.unitKcal = unitKcal;
    }

}
